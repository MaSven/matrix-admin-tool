/**
 *
 */
package space.smarquardt.matrixadmintool.httpapi.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Optional;

import javax.json.Json;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.net.ssl.SSLParameters;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpClient.Version;
import jdk.incubator.http.HttpHeaders;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import jdk.incubator.http.HttpResponse.BodyHandler;
import space.smarquardt.matrixadmintool.httpapi.Settings;
import space.smarquardt.matrixadmintool.httpapi.api.MatrixAdminRegister;
import space.smarquardt.matrixadmintool.httpapi.api.impl.MatrixAdminRegisterImpl;
import space.smarquardt.matrixadmintool.httpapi.requests.MatrixAdminRequest;
import space.smarquardt.matrixadmintool.httpapi.requests.MatrixRegisterAdminRequest;
import space.smarquardt.matrixadmintool.httpapi.response.MatrixAdminRegisterResponse;

/**
 * @author sven
 *
 */
@Ignore
class MatrixClientTest {

	private HttpClient httpClient;

	@BeforeClass
	public static void setSettings() throws MalformedURLException {
		Settings.setBaseUrl(URI.create("localhost").toURL());
	}

	@BeforeEach
	public void setUpClient() {
		this.httpClient = Mockito.mock(HttpClient.class);
	}

	/**
	 * Test method for
	 * {@link space.smarquardt.matrixadmintool.httpapi.client.MatrixClient#post(java.lang.String,
	 * , space.smarquardt.matrixadmintool.httpapi.CreatableFromJson)}.
	 * 
	 * @throws MatrixClientException
	 */
	@Test
	void testRegisterAdminUser() throws MatrixClientException {
		Jsonb jsonb = JsonbBuilder.create();
		final MatrixClient client = new MatrixClient(httpClient, jsonb);
		MatrixAdminRegister adminRegister = new MatrixAdminRegisterImpl(client);
		HttpResponse<String> userAdminReigstered = new HttpResponse<String>() {

			@Override
			public int statusCode() {
				return 202;
			}

			@Override
			public HttpRequest request() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Optional<HttpResponse<String>> previousResponse() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public HttpHeaders headers() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String body() {
				return Json.createObjectBuilder().add("access_token", "testtoken").add("user_id", "admin")
						.add("home_server", "matrix@testserver").build().toString();
			}

			@Override
			public SSLParameters sslParameters() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public URI uri() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Version version() {
				// TODO Auto-generated method stub
				return null;
			}
		};

		try {
			Mockito.when(
					httpClient.send(ArgumentMatchers.any(), ArgumentMatchers.any(BodyHandler.asString().getClass())))
					.thenReturn(userAdminReigstered);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MatrixRegisterAdminRequest adminRequest = new MatrixRegisterAdminRequest("admin", "passwort", "secrete");
		MatrixAdminRegisterResponse registerAdminUser = adminRegister.registerAdminUser(adminRequest);
		Assertions.assertThat(registerAdminUser.getAccessToken()).isNotNull();
		Assertions.assertThat(registerAdminUser.getHomeServer()).isEqualTo("matrix@testserver");
		Assertions.assertThat(registerAdminUser.getUserId()).isEqualTo("admin");
		Assertions.assertThat(JsonbBuilder.create().toJson(adminRequest))
				.isEqualTo(Json.createObjectBuilder().add("mac", "52dec027c71f167f35d363d75e8b947a3055c987")
						.add("password", "passwort").add("sharedSecrete", "secrete").add("user", "admin").build()
						.toString());

	}

	/**
	 * Test method for
	 * {@link space.smarquardt.matrixadmintool.httpapi.client.MatrixClient#get(java.lang.String, space.smarquardt.matrixadmintool.httpapi.CreatableFromJson)}.
	 */
	@Test
	@Ignore
	void testGet() {
		Assertions.fail("Not implemented");
	}

	/**
	 * Test method for
	 * {@link space.smarquardt.matrixadmintool.httpapi.client.MatrixClient#put(java.lang.String, space.smarquardt.matrixadmintool.httpapi.CreatableFromJson)}.
	 */
	@Test
	@Ignore
	void testPut() {
		Assertions.fail("Not implemented");
	}

}
