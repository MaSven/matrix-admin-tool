package space.smarquardt.matrixadmintool.httpapi.requests;

public class MatrixAdminRequest {

	private final String token;

	/**
	 * @param token
	 */
	public MatrixAdminRequest(final String token) {
		super();
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return this.token;
	}

}
