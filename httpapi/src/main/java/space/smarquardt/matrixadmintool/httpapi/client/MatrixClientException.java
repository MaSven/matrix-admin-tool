package space.smarquardt.matrixadmintool.httpapi.client;

public class MatrixClientException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3288406637281940150L;

	public MatrixClientException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MatrixClientException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MatrixClientException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MatrixClientException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MatrixClientException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
