package space.smarquardt.matrixadmintool.httpapi.requests;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author eiamnacken
 *
 */
public class MatrixRegisterAdminRequest {

	/**
	 * Algorithm used for hashing
	 */
	private static final String HASH_ALG = "HmacSha1";

	/**
	 * name of the admin
	 */
	private String user;

	/**
	 * new password for the admin user
	 */
	private String password;

	/**
	 * Is this user an admin always true for admin registration
	 */
	@JsonbProperty("admin")
	private final static boolean ADMIN = true;

	/**
	 * NULLByte used in the matrix server api to sperate the mac values
	 */
	private static final byte[] NULL_BYTE = new String("\u0000".getBytes(StandardCharsets.UTF_8),
			StandardCharsets.UTF_8).getBytes();

	/**
	 * hase value of the request
	 */
	private String sharedSecrete;
	private final static String TYPE = "org.matrix.login.shared_secret";

	/**
	 * @param name
	 * @param password
	 * @param sharedSecrete
	 */
	@JsonbCreator
	public MatrixRegisterAdminRequest(@JsonbProperty("user") final String name,@JsonbProperty("password") final String password,@JsonbProperty("sharedSecrete") final String sharedSecrete) {
		super();
		this.user = name;
		this.password = password;
		this.sharedSecrete = sharedSecrete;
	}

	/**
	 * @return the mac
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	@JsonbProperty("mac")
	public String getMac() throws NoSuchAlgorithmException, InvalidKeyException {
		Mac mac = Mac.getInstance(HASH_ALG);
		final SecretKeySpec keySpec = new SecretKeySpec(this.sharedSecrete.getBytes(), HASH_ALG);
		mac.init(keySpec);
		mac.update(this.user.getBytes());
		mac.update(NULL_BYTE);
		mac.update(this.password.getBytes());
		mac.update(NULL_BYTE);
		mac.update("admin".getBytes());
		return encodeHexString(mac.doFinal());
	}

	/**
	 * Converts an array of bytes into an array of characters representing the
	 * hexadecimal values of each byte in order. The returned array will be double
	 * the length of the passed array, as it takes two characters to represent any
	 * given byte.
	 *
	 * @param data a byte[] to convert to Hex characters
	 * @return A char[] containing lower-case hexadecimal characters
	 */
	public static char[] encodeHex(final byte[] data) {
		return encodeHex(data, true);
	}

	/**
	 * Used to build output as Hex
	 */
	private static final char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
			'e', 'f' };

	/**
	 * Used to build output as Hex
	 */
	private static final char[] DIGITS_UPPER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	/**
	 * Converts an array of bytes into an array of characters representing the
	 * hexadecimal values of each byte in order. The returned array will be double
	 * the length of the passed array, as it takes two characters to represent any
	 * given byte.
	 *
	 * @param data        a byte[] to convert to Hex characters
	 * @param toLowerCase <code>true</code> converts to lowercase,
	 *                    <code>false</code> to uppercase
	 * @return A char[] containing hexadecimal characters in the selected case
	 * @since 1.4
	 */
	public static char[] encodeHex(final byte[] data, final boolean toLowerCase) {
		return encodeHex(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
	}

	/**
	 * Converts an array of bytes into an array of characters representing the
	 * hexadecimal values of each byte in order. The returned array will be double
	 * the length of the passed array, as it takes two characters to represent any
	 * given byte.
	 *
	 * @param data     a byte[] to convert to Hex characters
	 * @param toDigits the output alphabet (must contain at least 16 chars)
	 * @return A char[] containing the appropriate characters from the alphabet For
	 *         best results, this should be either upper- or lower-case hex.
	 * @since 1.4
	 */
	protected static char[] encodeHex(final byte[] data, final char[] toDigits) {
		final int l = data.length;
		final char[] out = new char[l << 1];
		// two characters form the hex value.
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
			out[j++] = toDigits[0x0F & data[i]];
		}
		return out;
	}

	/**
	 * Converts an array of bytes into a String representing the hexadecimal values
	 * of each byte in order. The returned String will be double the length of the
	 * passed array, as it takes two characters to represent any given byte.
	 *
	 * @param data a byte[] to convert to Hex characters
	 * @return A String containing lower-case hexadecimal characters
	 * @since 1.4
	 */
	public static String encodeHexString(final byte[] data) {
		return new String(encodeHex(data));
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	public String getUser() {
		return user;
	}

	public String getSharedSecrete() {
		return sharedSecrete;
	}

	public static String getType() {
		return TYPE;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSharedSecrete(String sharedSecrete) {
		this.sharedSecrete = sharedSecrete;
	}
	
	

}
