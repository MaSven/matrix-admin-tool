package space.smarquardt.matrixadmintool.httpapi.client;

import java.io.IOException;
import java.net.URI;
import java.util.function.Function;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpRequest.BodyPublisher;
import jdk.incubator.http.HttpResponse;
import jdk.incubator.http.HttpResponse.BodyHandler;
import space.smarquardt.matrixadmintool.httpapi.Settings;

public class MatrixClient {

	private final HttpClient httpClient;

	private final Jsonb jsonBuilder;

	/**
	 * Creates the URI endpont to get called
	 *
	 * @param endpoint Endpoint to call. This gets appended to the base url of the
	 *                 matrix server
	 * @return
	 */
	private URI createURI(final String endpoint) {
		return URI.create(Settings.getBaseUrl().toString() + endpoint);
	}

	/**
	 * Sends an POST to the matrix server. The URL called is the baseurl of the
	 * matrixserver plus the given url
	 *
	 * @param url                       The url to call
	 * @param jsonObject                The object we want to send to the server
	 * @param responseBodyToReturnValue {@link Function} that takes the response as
	 *                                  jsonString and turns it into the requested
	 *                                  object
	 * @param responseClass             Class which is expected to be returned of
	 *                                  the post
	 * 
	 * @return
	 * @throws MatrixClientException
	 */
	public <T, E> T post(final String url, final E jsonObject, Class<T> responseClass) throws MatrixClientException {
		String response = this.call(url,
				(uri) -> HttpRequest.newBuilder(uri).headers("Content-Type", "application/json")
						.POST(BodyPublisher.fromString(this.jsonBuilder.toJson(jsonObject))).build());
		return this.jsonBuilder.fromJson(response, responseClass);

	}

	/**
	 * Call the matrix server on the specified endpoint with the given request
	 * function
	 * 
	 * @param endpoint Endpoint of the matrixserver url
	 * @param caller   Caller which builds the {@link HttpRequest} for the call
	 * @return The returned body as String
	 * @throws MatrixClientException If an httperror occurs during the call
	 */
	private String call(final String endpoint, final Function<URI, HttpRequest> caller) throws MatrixClientException {
		URI urlToCall = this.createURI(endpoint);
		HttpResponse<String> response;
		try {
			response = this.httpClient.send(caller.apply(urlToCall), BodyHandler.asString());
			if (response.statusCode() >= 400 && response.statusCode() < 500) {
				throw new MatrixClientException(
						"Error on the client side. Statuscode " + response.statusCode() + " retrieved");
			} else if (response.statusCode() >= 500) {
				throw new MatrixClientException(
						"Error on the server side. Statuscode " + response.statusCode() + " retrieved");
			}
			return response.body();
		} catch (IOException | InterruptedException e) {
			throw new MatrixClientException(e);
		}

	}

	/**
	 * @param url
	 * @param responseClass
	 * @return
	 * @throws MatrixClientException
	 */
	public <T> T get(final String url, Class<T> responseClass) throws MatrixClientException {
		String reponseBody = this.call(url,
				(uri) -> HttpRequest.newBuilder(uri).GET().header("Content-Type", "application/json").build());
		return this.jsonBuilder.fromJson(reponseBody, responseClass);
	}

	/**
	 * @param url
	 * @param jsonObject
	 * @param responseClass
	 * @return
	 * @throws MatrixClientException
	 */
	public <T, E> T put(final String url, final E jsonObject, Class<T> responseClass) throws MatrixClientException {
		String responseBody = this.call(url,
				(uri) -> HttpRequest.newBuilder(uri).header("Content-Type", "application/json")
						.POST(BodyPublisher.fromString(this.jsonBuilder.toJson(jsonObject))).build());
		return this.jsonBuilder.fromJson(responseBody, responseClass);
	}

	/**
	 * Create httpclient with own configuration
	 *
	 * @param client The {@link HttpClient}
	 */
	public MatrixClient(final HttpClient client, Jsonb jsonBuilder) {
		this.httpClient = client;
		this.jsonBuilder = jsonBuilder;
	}

	/**
	 * Create new Matrixclient with the default {@link HttpClient#newHttpClient()}
	 */
	public MatrixClient() {
		this(HttpClient.newHttpClient(), JsonbBuilder.create());
	}

}
