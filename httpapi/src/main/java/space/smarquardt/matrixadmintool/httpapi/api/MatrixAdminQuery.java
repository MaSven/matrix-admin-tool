package space.smarquardt.matrixadmintool.httpapi.api;

import space.smarquardt.matrixadmintool.httpapi.requests.MatrixAdminRequest;
import space.smarquardt.matrixadmintool.httpapi.response.MatrixResponseWhoIs;

public interface MatrixAdminQuery {

	/**
	 * Get information of the user
	 *
	 * @param userId
	 * @param adminRequest
	 * @return
	 */
	public MatrixResponseWhoIs searchUser(String userId, MatrixAdminRequest adminRequest);

	/**
	 * Deactivate a user
	 *
	 * @param userId
	 * @param adminRequest
	 */
	public void deactivateUser(final String userId, MatrixAdminRequest adminRequest);

	/**
	 * Reset a password
	 * 
	 * @param userId
	 * @param adminRequest
	 */
	public void resetPassword(String userId, MatrixAdminRequest adminRequest);

}
