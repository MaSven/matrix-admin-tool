package space.smarquardt.matrixadmintool.httpapi.api.impl;

import java.net.URI;

import space.smarquardt.matrixadmintool.httpapi.Settings;
import space.smarquardt.matrixadmintool.httpapi.api.MatrixAdminRegister;
import space.smarquardt.matrixadmintool.httpapi.client.MatrixClient;
import space.smarquardt.matrixadmintool.httpapi.client.MatrixClientException;
import space.smarquardt.matrixadmintool.httpapi.requests.MatrixRegisterAdminRequest;
import space.smarquardt.matrixadmintool.httpapi.response.MatrixAdminRegisterResponse;

/**
 * @author eiamnacken
 *
 */

public class MatrixAdminRegisterImpl implements MatrixAdminRegister {

	/**
	 * URL endpoint to register a user
	 */
	private static final String REGISTER_ADMIN_URL = "/_matrix/client/api/v1/register";

	/**
	 * Cliet for sending to the matrix server
	 */
	private MatrixClient client;

	public MatrixAdminRegisterImpl(MatrixClient client) {
		super();
		this.client = client;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see space.smarquardt.matrixadmintool.httpapi.api.MatrixAdminRegister#
	 * registerAdminUser(space.smarquardt.matrixadmintool.httpapi.requests.
	 * MatrixRegisterAdminRequest)
	 */
	@Override
	public MatrixAdminRegisterResponse registerAdminUser(final MatrixRegisterAdminRequest adminRequest)
			throws MatrixClientException {
		return this.client.post(REGISTER_ADMIN_URL, adminRequest, MatrixAdminRegisterResponse.class);
	}

}
