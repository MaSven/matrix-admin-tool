package space.smarquardt.matrixadmintool.httpapi.response;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class MatrixAdminRegisterResponse {
	@JsonbProperty("access_token")
	private  String accessToken;
	@JsonbProperty("home_server")
	private  String homeServer;
	@JsonbProperty("user_id")
	private  String userId;

	
	

	public MatrixAdminRegisterResponse() {
		super();
	}

	/**
	 * @param accessToken
	 * @param homeServer
	 * @param userId
	 */
	@JsonbCreator
	public MatrixAdminRegisterResponse(@JsonbProperty("access_token") final String accessToken,@JsonbProperty("home_server") final String homeServer,@JsonbProperty("user_id") final String userId) {
		super();
		this.accessToken = accessToken;
		this.homeServer = homeServer;
		this.userId = userId;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return this.accessToken;
	}

	/**
	 * @return the homeServer
	 */
	public String getHomeServer() {
		return this.homeServer;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return this.userId;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setHomeServer(String homeServer) {
		this.homeServer = homeServer;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
