package space.smarquardt.matrixadmintool.httpapi.response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MatrixResponseWhoIs {

	private final String userId;

	List<Device> devices;

	/**
	 * @param userId
	 */
	public MatrixResponseWhoIs(final String userId) {
		super();
		this.userId = userId;
		this.devices = new ArrayList<>();
	}

	public void addDevice(final Device device) {
		this.devices.add(device);
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * @return the devices
	 */
	public List<Device> getDevices() {
		return this.devices;
	}

	public class Device {
		private final String name;
		private final List<Session> sessions;

		/**
		 * @param name
		 */
		public Device(final String name) {
			super();
			this.name = name;
			this.sessions = new ArrayList<>();
		}

		public void addAllSessions(final Collection<Session> collection) {
			this.sessions.addAll(collection);
		}

		public void addSession(final Session session) {
			this.sessions.add(session);
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * @return the sessions
		 */
		public List<Session> getSessions() {
			return this.sessions;
		}

	}

	public class Session {
		List<Connection> connections;

		public Session() {
			this.connections = new ArrayList<>();
		}

		public void addConnection(final Connection connection) {
			this.connections.add(connection);
		}

		/**
		 * @return the connections
		 */
		public List<Connection> getConnections() {
			return this.connections;
		}

	}

	public class Connection {
		private final String ip;
		private final long lastSeen;
		private final String userAgent;

		/**
		 * @param ip
		 * @param lastSeen
		 * @param userAgent
		 */
		public Connection(final String ip, final long lastSeen, final String userAgent) {
			super();
			this.ip = ip;
			this.lastSeen = lastSeen;
			this.userAgent = userAgent;
		}

		/**
		 * @return the ip
		 */
		public String getIp() {
			return this.ip;
		}

		/**
		 * @return the lastSeen
		 */
		public long getLastSeen() {
			return this.lastSeen;
		}

		/**
		 * @return the userAgent
		 */
		public String getUserAgent() {
			return this.userAgent;
		}

	}

	

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("MatrixResponseWhoIs [");
		if (this.userId != null) {
			builder.append("userId=");
			builder.append(this.userId);
			builder.append(", ");
		}
		if (this.devices != null) {
			builder.append("devices=");
			builder.append(this.devices);
		}
		builder.append("]");
		return builder.toString();
	}

}
