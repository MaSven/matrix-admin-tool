package space.smarquardt.matrixadmintool.httpapi.api;

import space.smarquardt.matrixadmintool.httpapi.client.MatrixClientException;
import space.smarquardt.matrixadmintool.httpapi.requests.MatrixRegisterAdminRequest;
import space.smarquardt.matrixadmintool.httpapi.response.MatrixAdminRegisterResponse;

public interface MatrixAdminRegister {

	/**
	 * Register a new user as admin
	 */
	MatrixAdminRegisterResponse registerAdminUser(MatrixRegisterAdminRequest adminRequest) throws MatrixClientException;
}
