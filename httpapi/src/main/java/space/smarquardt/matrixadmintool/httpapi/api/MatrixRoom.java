package space.smarquardt.matrixadmintool.httpapi.api;

import space.smarquardt.matrixadmintool.httpapi.requests.MatrixAdminRequest;

public interface MatrixRoom {

	/**
	 * Deletes all events of the given room till the given timestampt
	 *
	 * @param timestamp
	 * @param deleteLocalEvents
	 * @param roomId
	 * @param admin
	 */
	void purgeHistory(long timestamp, boolean deleteLocalEvents, long roomId, MatrixAdminRequest admin);

	void createRoom();

}
