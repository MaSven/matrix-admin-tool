package space.smarquardt.matrixadmintool.httpapi.requests;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

import space.smarquardt.matrixadmintool.httpapi.Settings;

public class MatrixLoginRequest {
	
	/**
	 * Type of the login. We want token logins only
	 */
	@JsonbProperty("type")
	private final static String TYPE="m.login.token";
	
	/**
	 * Token for this login
	 */
	private final String token;
	
	private String user;
	
	/**
	 * Device name of the login
	 */
	@JsonbProperty("initial_device_display_name")
	private String initialDeviceDisplayName;
	@JsonbCreator
	public MatrixLoginRequest(@JsonbProperty("token") String token,@JsonbProperty("user") String user) {
		super();
		this.token = token;
		this.user = user;
		this.initialDeviceDisplayName = Settings.DEVICE_ID;
	}
	public static String getType() {
		return TYPE;
	}
	public String getToken() {
		return token;
	}
	public String getUser() {
		return user;
	}
	public String getInitialDeviceDisplayName() {
		return initialDeviceDisplayName;
	}
	
	
	
	
	

}
