package space.smarquardt.matrixadmintool.httpapi.requests;

public class NewPasswordRequest {

	private final String newPassword;

	/**
	 * @param newPassword
	 */
	public NewPasswordRequest(final String newPassword) {
		super();
		this.newPassword = newPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return this.newPassword;
	}

	

}
