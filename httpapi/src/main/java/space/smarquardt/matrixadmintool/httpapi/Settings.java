package space.smarquardt.matrixadmintool.httpapi;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Optional;

public class Settings {

	private static URL baseUrl;
	
	/**
	 * Device id of this matrix client
	 */
	public final static String DEVICE_ID = "Matrix-Admin-Tool";

	public Settings() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the baseUrl
	 */
	public static URL getBaseUrl() {
		try {
			return Optional.ofNullable(baseUrl).orElse(URI.create("http://localhost").toURL());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new RuntimeException();
		
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public static void setBaseUrl(final URL baseUrl) {
		Settings.baseUrl = baseUrl;
	}

}
