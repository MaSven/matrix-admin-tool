/**
 *
 */
/**
 * @author sven
 *
 */
module space.smarquardt.matrixadmintool.httpapi {
	exports space.smarquardt.matrixadmintool.httpapi;
	exports space.smarquardt.matrixadmintool.httpapi.api;
	exports space.smarquardt.matrixadmintool.httpapi.api.impl;
	exports space.smarquardt.matrixadmintool.httpapi.requests;
	exports space.smarquardt.matrixadmintool.httpapi.response;
	
	opens space.smarquardt.matrixadmintool.httpapi.response;
	opens space.smarquardt.matrixadmintool.httpapi.client;
	
	requires java.base;
	requires jdk.incubator.httpclient;
	requires java.json.bind;
	requires static java.json;

}