#!/usr/bin/env bash
git clone  https://github.com/matrix-org/matrix-doc.git
cd matrix-doc
git checkout tags/client_server/r0.3.0
virtualenv env
env/bin/pip install -r scripts/requirements.txt
source env/bin/activate
./scripts/dump-swagger.py -c r0.3.0
mv scripts/swagger/api-docs.json ../
rm -rf env ;
cd .. ;
rm -rf matrix-doc;

